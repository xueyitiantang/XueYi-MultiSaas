package com.xueyi.auth.support.wechatMa;

import com.xueyi.auth.support.base.AuthenticationBaseToken;
import com.xueyi.common.core.constant.basic.SecurityConstants;
import org.springframework.security.core.Authentication;

import java.util.Map;
import java.util.Set;

/**
 * 自定义授权模式 | 微信小程序模式
 *
 * @author xueyi
 */
public class AuthenticationWechatMaToken extends AuthenticationBaseToken {

    public AuthenticationWechatMaToken(Authentication clientPrincipal, Set<String> scopes, Map<String, Object> additionalParameters) {
        super(SecurityConstants.GrantType.WECHAT_MA, clientPrincipal, scopes, additionalParameters);
    }
}
