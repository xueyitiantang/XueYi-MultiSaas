update sys_dict_type set id = 101001001, name = '默认|通用字典|性别', remark= '默认|通用字典|性别列表' where id = 10001;
update sys_dict_type set id = 101001002, name = '默认|通用字典|显隐', remark= '默认|通用字典|显隐列表' where id = 10002;
update sys_dict_type set id = 101001003, name = '默认|通用字典|公共私有', remark= '默认|通用字典|公共私有列表' where id = 10003;
update sys_dict_type set id = 101001004, name = '默认|通用字典|状态', remark= '默认|通用字典|状态列表' where id = 10004;
update sys_dict_type set id = 101001005, name = '默认|通用字典|功能状态', remark= '默认|通用字典|功能状态列表' where id = 10032;
update sys_dict_type set id = 101001007, name = '默认|通用字典|是否', remark= '默认|通用字典|是否列表' where id = 10009;
update sys_dict_type set id = 101001008, name = '默认|通用字典|消息状态', remark= '默认|通用字典|消息状态列表' where id = 10015;


update sys_dict_type set id = 102001001 where id = 10010;
update sys_dict_type set id = 102001002 where id = 10011;
update sys_dict_type set id = 102001003 where id = 10012;
update sys_dict_type set id = 102001004 where id = 10013;
update sys_dict_type set id = 102001005 where id = 10014;
update sys_dict_type set id = 102001006 where id = 10016;
update sys_dict_type set id = 102001007 where id = 10017;
update sys_dict_type set id = 102001008 where id = 10018;
update sys_dict_type set id = 102001009 where id = 10019;
update sys_dict_type set id = 102001010 where id = 10020;
update sys_dict_type set id = 102001011 where id = 10030;
update sys_dict_type set id = 102001012 where id = 10031;

update sys_dict_type set id = 103001001 where id = 10033;


update sys_dict_type set id = 104001001 where id = 10005;
update sys_dict_type set id = 104001002 where id = 10006;
update sys_dict_type set id = 104001003 where id = 10007;
update sys_dict_type set id = 104001004 where id = 10008;


update sys_dict_type set id = 105001001 where id = 10021;
update sys_dict_type set id = 105001002 where id = 10022;
update sys_dict_type set id = 105001003 where id = 10023;
update sys_dict_type set id = 105001004 where id = 10024;
update sys_dict_type set id = 105001005 where id = 10025;
update sys_dict_type set id = 105001006 where id = 10026;
update sys_dict_type set id = 105001007 where id = 10027;
update sys_dict_type set id = 105001008 where id = 10028;
update sys_dict_type set id = 105001009 where id = 10029;




update sys_dict_data set id = 101001001001, remark= '默认|通用字典|性别：男' where id =   1000101;
update sys_dict_data set id = 101001001002, remark= '默认|通用字典|性别：女' where id =   1000102;
update sys_dict_data set id = 101001001003, remark= '默认|通用字典|性别：未知' where id =   1000103;
update sys_dict_data set id = 101001002001, remark= '默认|通用字典|显隐：显示' where id =   1000201;
update sys_dict_data set id = 101001002002, remark= '默认|通用字典|显隐：隐藏' where id =   1000202;
update sys_dict_data set id = 101001003001, remark= '默认|通用字典|公共私有：公共' where id =   1000301;
update sys_dict_data set id = 101001003002, remark= '默认|通用字典|公共私有：私有' where id =   1000302;
update sys_dict_data set id = 101001004001, remark= '默认|通用字典|状态：正常' where id =   1000401;
update sys_dict_data set id = 101001004002, remark= '默认|通用字典|状态：停用' where id =   1000402;
update sys_dict_data set id = 101001005001, remark= '默认|通用字典|功能状态：开启' where id =   1003201;
update sys_dict_data set id = 101001005002, remark= '默认|通用字典|功能状态：关闭' where id =   1003202;
update sys_dict_data set id = 101001007001, remark= '默认|通用字典|是否：是' where id =   1000901;
update sys_dict_data set id = 101001007002, remark= '默认|通用字典|是否：否' where id =   1000902;
update sys_dict_data set id = 101001008001, remark= '默认|通用字典|消息状态：成功' where id =   1001501;
update sys_dict_data set id = 101001008002, remark= '默认|通用字典|消息状态：失败' where id =   1001502;


update sys_dict_data set id = 102001001001 where id =   1001001;
update sys_dict_data set id = 102001001002 where id =   1001002;
update sys_dict_data set id = 102001002001 where id =   1001101;
update sys_dict_data set id = 102001002002 where id =   1001102;
update sys_dict_data set id = 102001002003 where id =   1001103;
update sys_dict_data set id = 102001002004 where id =   1001104;
update sys_dict_data set id = 102001002005 where id =   1001105;

update sys_dict_data set id = 102001003001 where id =   1001201;
update sys_dict_data set id = 102001003002 where id =   1001202;
update sys_dict_data set id = 102001003003 where id =   1001203;
update sys_dict_data set id = 102001003004 where id =   1001204;
update sys_dict_data set id = 102001003005 where id =   1001205;
update sys_dict_data set id = 102001003006 where id =   1001206;
update sys_dict_data set id = 102001003007 where id =   1001207;
update sys_dict_data set id = 102001003008 where id =   1001208;
update sys_dict_data set id = 102001003009 where id =   1001209;
update sys_dict_data set id = 102001003010 where id =   1001210;
update sys_dict_data set id = 102001003011 where id =   1001211;
update sys_dict_data set id = 102001003012 where id =   1001212;
update sys_dict_data set id = 102001003013 where id =   1001213;
update sys_dict_data set id = 102001003014 where id =   1001214;
update sys_dict_data set id = 102001003015 where id =   1001215;
update sys_dict_data set id = 102001003016 where id =   1001216;
update sys_dict_data set id = 102001003017 where id =   1001217;


update sys_dict_data set id = 102001004001 where id =   1001301;
update sys_dict_data set id = 102001004002 where id =   1001302;
update sys_dict_data set id = 102001004003 where id =   1001303;


update sys_dict_data set id = 102001005001 where id =   1001401;
update sys_dict_data set id = 102001005002 where id =   1001402;


update sys_dict_data set id = 102001006001 where id =   1001601;
update sys_dict_data set id = 102001006002 where id =   1001602;
update sys_dict_data set id = 102001006003 where id =   1001603;
update sys_dict_data set id = 102001006004 where id =   1001604;
update sys_dict_data set id = 102001006005 where id =   1001605;
update sys_dict_data set id = 102001006006 where id =   1001606;
update sys_dict_data set id = 102001006007 where id =   1001607;
update sys_dict_data set id = 102001006008 where id =   1001608;

update sys_dict_data set id = 102001007001 where id =   1001701;
update sys_dict_data set id = 102001007002 where id =   1001702;
update sys_dict_data set id = 102001007003 where id =   1001703;
update sys_dict_data set id = 102001007004 where id =   1001704;
update sys_dict_data set id = 102001007005 where id =   1001705;

update sys_dict_data set id = 102001008001 where id =   1001801;
update sys_dict_data set id = 102001008002 where id =   1001802;
update sys_dict_data set id = 102001008003 where id =   1001803;

update sys_dict_data set id = 102001009001 where id =   1001901;
update sys_dict_data set id = 102001009002 where id =   1001902;
update sys_dict_data set id = 102001009003 where id =   1001903;
update sys_dict_data set id = 102001009004 where id =   1001904;

update sys_dict_data set id = 102001010001 where id =   1002001;
update sys_dict_data set id = 102001010002 where id =   1002002;
update sys_dict_data set id = 102001010003 where id =   1002003;
update sys_dict_data set id = 102001010004 where id =   1002004;
update sys_dict_data set id = 102001010005 where id =   1002005;
update sys_dict_data set id = 102001010006 where id =   1002006;

update sys_dict_data set id = 102001011001 where id =   1003001;
update sys_dict_data set id = 102001011002 where id =   1003002;
update sys_dict_data set id = 102001011003 where id =   1003003;
update sys_dict_data set id = 102001011004 where id =   1003004;

update sys_dict_data set id = 102001012001 where id =   1003101;
update sys_dict_data set id = 102001012002 where id =   1003102;

update sys_dict_data set id = 103001001001 where id =   1003301;

update sys_dict_data set id = 104001001001 where id =   1000501;
update sys_dict_data set id = 104001001002 where id =   1000502;

update sys_dict_data set id = 104001002001 where id =   1000601;
update sys_dict_data set id = 104001002002 where id =   1000602;
update sys_dict_data set id = 104001002003 where id =   1000603;
update sys_dict_data set id = 104001002004 where id =   1000604;

update sys_dict_data set id = 104001003001 where id =   1000701;
update sys_dict_data set id = 104001003002 where id =   1000702;

update sys_dict_data set id = 104001004001 where id =   1000801;
update sys_dict_data set id = 104001004002 where id =   1000802;
update sys_dict_data set id = 104001004003 where id =   1000803;


update sys_dict_data set id = 105001001001 where id =   1002101;
update sys_dict_data set id = 105001001002 where id =   1002102;
update sys_dict_data set id = 105001001003 where id =   1002103;

update sys_dict_data set id = 105001002001 where id =   1002201;
update sys_dict_data set id = 105001002002 where id =   1002202;

update sys_dict_data set id = 105001003001 where id =   1002301;
update sys_dict_data set id = 105001003002 where id =   1002302;

update sys_dict_data set id = 105001004001 where id =   1002401;
update sys_dict_data set id = 105001004002 where id =   1002402;
update sys_dict_data set id = 105001004003 where id =   1002403;

update sys_dict_data set id = 105001005001 where id =   1002501;
update sys_dict_data set id = 105001005002 where id =   1002502;
update sys_dict_data set id = 105001005003 where id =   1002503;
update sys_dict_data set id = 105001005004 where id =   1002504;
update sys_dict_data set id = 105001005005 where id =   1002505;
update sys_dict_data set id = 105001005006 where id =   1002506;

update sys_dict_data set id = 105001006001 where id =   1002601;
update sys_dict_data set id = 105001006002 where id =   1002602;
update sys_dict_data set id = 105001006003 where id =   1002603;
update sys_dict_data set id = 105001006004 where id =   1002604;
update sys_dict_data set id = 105001006005 where id =   1002605;
update sys_dict_data set id = 105001006006 where id =   1002606;
update sys_dict_data set id = 105001006007 where id =   1002607;
update sys_dict_data set id = 105001006008 where id =   1002608;
update sys_dict_data set id = 105001006009 where id =   1002609;
update sys_dict_data set id = 105001006010 where id =   1002610;
update sys_dict_data set id = 105001006011 where id =   1002611;
update sys_dict_data set id = 105001006012 where id =   1002612;
update sys_dict_data set id = 105001006013 where id =   1002613;
update sys_dict_data set id = 105001006014 where id =   1002614;
update sys_dict_data set id = 105001006015 where id =   1002615;
update sys_dict_data set id = 105001006016 where id =   1002616;

update sys_dict_data set id = 105001007001 where id =   1002701;
update sys_dict_data set id = 105001007002 where id =   1002702;
update sys_dict_data set id = 105001007003 where id =   1002703;
update sys_dict_data set id = 105001007004 where id =   1002704;
update sys_dict_data set id = 105001007005 where id =   1002705;
update sys_dict_data set id = 105001007006 where id =   1002706;
update sys_dict_data set id = 105001007007 where id =   1002707;
update sys_dict_data set id = 105001007008 where id =   1002708;
update sys_dict_data set id = 105001007009 where id =   1002709;
update sys_dict_data set id = 105001007010 where id =   1002710;
update sys_dict_data set id = 105001007011 where id =   1002711;
update sys_dict_data set id = 105001007012 where id =   1002712;
update sys_dict_data set id = 105001007013 where id =   1002713;
update sys_dict_data set id = 105001007014 where id =   1002714;
update sys_dict_data set id = 105001007015 where id =   1002715;
update sys_dict_data set id = 105001007016 where id =   1002716;

update sys_dict_data set id = 105001008001 where id =   1002801;
update sys_dict_data set id = 105001008002 where id =   1002802;

update sys_dict_data set id = 105001009001 where id =   1002901;
update sys_dict_data set id = 105001009002 where id =   1002902;

insert into sys_dict_type (id, name, code, remark, data_type, cache_type, tenant_id)
values (104001005, '定时任务|内部系统类型', 'sys_job_inner_type', '定时任务|内部系统类型列表', '0', '1', 0);

insert into sys_dict_data (id, sort, label, value, code, css_class, list_class, is_default, remark, tenant_id)
values (104001005001, 1, '系统服务', 'SYSTEM', 'sys_job_inner_type', '', 'blue', 'N', '定时任务|内部系统类型：系统服务：xueyi-system：DEFAULT_GROUP', 0),
       (104001005002, 2, '租户服务', 'TENANT', 'sys_job_inner_type', '', 'green', 'N', '定时任务|内部系统类型：租户服务：xueyi-tenant：DEFAULT_GROUP', 0);

insert into sys_dict_type (id, name, code, remark, data_type, cache_type, tenant_id)
values (101001006, '默认|通用字典|请求类型', 'sys_http_type', '默认|通用字典|请求类型列表', '3', '1', 0);

insert into sys_dict_data (id, sort, label, value, code, css_class, list_class, is_default, remark, tenant_id)
values (101001006001, 1, 'GET', 'GET', 'sys_http_type', '', 'blue', 'Y', '默认|通用字典|请求类型：GET', 0),
       (101001006002, 2, 'POST', 'POST', 'sys_http_type', '', 'blue', 'Y', '默认|通用字典|请求类型：POST', 0),
       (101001006003, 3, 'PUT', 'PUT', 'sys_http_type', '', 'blue', 'Y', '默认|通用字典|请求类型：PUT', 0),
       (101001006004, 4, 'DEL', 'DEL', 'sys_http_type', '', 'blue', 'Y', '默认|通用字典|请求类型：DEL', 0);

alter table sys_job
    modify job_group varchar(64) not null comment '任务组名';

alter table sys_job
    add server_type varchar(12) null comment '归属服务' after job_group;

alter table sys_job
    add http_type varchar(12) null comment '请求类型' after server_type;

alter table sys_job
    add api_url varchar(500) null comment '请求地址' after http_type;

alter table sys_job
    modify invoke_target varchar(500) null comment '调用目标字符串';
alter table sys_job_log
    add server_type varchar(12) null comment '归属服务' after job_group;

alter table sys_job_log
    add http_type varchar(12) null comment '请求类型' after server_type;

alter table sys_job_log
    add api_url varchar(500) null comment '请求地址' after http_type;


alter table sys_job_log
    modify invoke_target varchar(500) null comment '调用目标字符串';



delete from sys_dict_data where code = 'sys_job_group';

insert into sys_dict_data (id, sort, label, value, code, css_class, list_class, is_default, remark, tenant_id)
values (104001004001, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '定时任务|任务分组：默认', 0),
       (104001004002, 2, '内部系统', 'INNER_SYSTEM', 'sys_job_group', '', '', 'N', '定时任务|任务分组：内部系统', 0),
       (104001004003, 3, '外部系统', 'EXTERNAL_SYSTEM', 'sys_job_group', '', '', 'N', '定时任务|任务分组：外部系统', 0);
