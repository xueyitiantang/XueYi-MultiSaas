/*
 Navicat Premium Dump SQL

 Source Server         : 192.168.31.100
 Source Server Type    : MySQL
 Source Server Version : 80031 (8.0.31)
 Source Host           : 192.168.31.100:3306
 Source Schema         : nacos

 Target Server Type    : MySQL
 Target Server Version : 80031 (8.0.31)
 File Encoding         : 65001

 Date: 05/03/2025 21:55:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for config_info
-- ----------------------------
DROP TABLE IF EXISTS `config_info`;
CREATE TABLE `config_info`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT 'group_id',
  `content` longtext CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT 'content',
  `md5` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL COMMENT 'source user',
  `src_ip` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT 'source ip',
  `app_name` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT 'app_name',
  `tenant_id` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT '' COMMENT '租户字段',
  `c_desc` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT 'configuration description',
  `c_use` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT 'configuration usage',
  `effect` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT '配置生效的描述',
  `type` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT '配置的类型',
  `c_schema` text CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL COMMENT '配置的模式',
  `encrypted_data_key` varchar(1024) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL DEFAULT '' COMMENT '密钥',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfo_datagrouptenant`(`data_id` ASC, `group_id` ASC, `tenant_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_bin COMMENT = 'config_info' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_info
-- ----------------------------
INSERT INTO `config_info` VALUES (1, 'application-dev.yml', 'DEFAULT_GROUP', 'spring:\n  # feign 配置\n  cloud:\n    openfeign:\n      okhttp:\n        enabled: true\n      httpclient:\n        enabled: false\n      client:\n        config:\n          default:\n            connectTimeout: 10000\n            readTimeout: 10000\n      compression:\n        request:\n          enabled: true\n        response:\n          enabled: true\nfeign:\n  sentinel:\n    enabled: true\n\n# 暴露监控端点\nmanagement:\n  endpoints:\n    web:\n      exposure:\n        include: \' *\'\n\nxueyi:\n  # 安全配置\n  security:\n    # 密钥\n    secret:\n      # 网关地址\n      gatewayUrl: ${secret.security.secret.gatewayUrl}\n      # 令牌秘钥\n      token: ${secret.security.secret.token}', 'ae8cb093333b01ad03ce87d8ebc554cc', '2025-03-05 21:29:52', '2025-03-05 21:49:38', 'nacos', '192.168.31.128', '', 'a3d0a606-52e7-43e3-a194-6052aa159aff', '通用参数配置', '', '', 'yaml', '', '');
INSERT INTO `config_info` VALUES (2, 'application-secret-dev.yml', 'DEFAULT_GROUP', 'secret:\n  # redis参数信息\n  redis:\n    host: 127.0.0.1\n    port: 6379\n    password:\n    database: 0\n  # 安全配置信息\n  security:\n    secret:\n      # 网关地址\n      gatewayUrl: http://localhost:8080\n      # 令牌秘钥\n      token: ce6e6cc15d0349b58f9d8c43e5f328ce7f32902943asfafafwfwg5341475754awawfaf7541813775843afafa845eh78d614cc2e63779489d9a2aaaafd674c5515e708ba5869a4dcfa3e66799\n  # 服务状态监控参数信息\n  monitor:\n    name: xueyi\n    password: xueyi123\n    title: 服务状态监控\n  # swagger参数信息\n  swagger:\n    author:\n      name: xueyi\n      email: xueyitt@qq.com\n    version: v3.0.2\n    title: 接口文档\n    license: Powered By xueyi\n    licenseUrl: https://doc.xueyitt.cn\n  # datasource主库参数信息\n  datasource:\n    driver-class-name: com.mysql.cj.jdbc.Driver\n    url: jdbc:mysql://localhost:3306/xy-cloud?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&allowMultiQueries=true&serverTimezone=GMT%2B8\n    username: root\n    password: password\n  # nacos参数信息\n  nacos:\n    serverAddr: 127.0.0.1:8848\n    namespace:\n    username:\n    password:\n  # RabbitMQ 配置\n  rabbitmq:\n    # 默认地址\n    host: 127.0.0.1\n    # 默认端口\n    port: 5672\n    # 账号\n    username: admin\n    # 密码\n    password: password    # 前缀标识\n    prefix: xueyitt    # 环境标识 最终生成/访问的MQ exchange|queue 格式为：前缀标识.环境标识.自定义exchange|queue名称 如：xueyitt.dev.xxxxx\n    evn: dev', '59e183f29703253e9bbb30dd4a47cacf', '2025-03-05 21:32:45', '2025-03-05 21:50:36', 'nacos', '192.168.31.128', '', 'a3d0a606-52e7-43e3-a194-6052aa159aff', '通用参数配置', '', '', 'yaml', '', '');
INSERT INTO `config_info` VALUES (3, 'application-datasource-dev.yml', 'DEFAULT_GROUP', '# spring配置\nspring:\n  data:\n    redis:\n      host: ${secret.redis.host}\n      port: ${secret.redis.port}\n      password: ${secret.redis.password}\n      database: ${secret.redis.database}\n  datasource:\n    dynamic:\n      hikari:\n        # 连接超时时间：毫秒\n        connection-timeout: 30000\n        # 连接测试活性最长时间：毫秒\n        validation-timeout: 5000\n        # 空闲连接最大存活时间\n        idle-timeout: 300000\n        # 连接最大存活时间\n        max-lifetime: 600000\n        # 最大连接数\n        max-pool-size: 20\n        # 最小空闲连接\n        min-idle: 10\n      datasource:\n          # 主库数据源\n          master:\n            driver-class-name: ${secret.datasource.driver-class-name}\n            url: ${secret.datasource.url}\n            username: ${secret.datasource.username}\n            password: ${secret.datasource.password}\n          # 数据源信息会通过master库进行获取并生成，请在主库的xy_tenant_source中配置即可\n      # seata: true    # 开启seata代理，开启后默认每个数据源都代理，如果某个不需要代理可单独关闭\n\n# mybatis-plus配置\nmybatis-plus:\n  global-config:\n    # 是否控制台 print mybatis-plus 的 LOGO\n    banner: false\n    db-config:\n      # 字段验证策略之 select\n      selectStrategy: NOT_EMPTY\n      # 字段验证策略之 insert\n      insertStrategy: NOT_NULL\n      # 字段验证策略之 update\n      updateStrategy: IGNORED\n      # 全局逻辑删除的实体字段名\n      logic-delete-field: delFlag\n      # 逻辑已删除值\n      logic-delete-value: 1\n      # 逻辑未删除值\n      logic-not-delete-value: 0\n  configuration:\n    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl\n\n# seata配置\nseata:\n  # 默认关闭，如需启用spring.datasource.dynami.seata需要同时开启\n  enabled: false\n  # Seata 应用编号，默认为 ${spring.application.name}\n  application-id: ${spring.application.name}\n  # Seata 事务组编号，用于 TC 集群名\n  tx-service-group: ${spring.application.name}-group\n  # 关闭自动代理\n  enable-auto-data-source-proxy: false\n  config:\n    type: nacos\n    nacos:\n      serverAddr: ${secret.nacos.serverAddr}\n      namespace: ${secret.nacos.namespace}\n      username: ${secret.nacos.username}\n      password: ${secret.nacos.password}\n      group: SEATA_GROUP\n  registry:\n    type: nacos\n    nacos:\n      application: seata-server\n      server-addr: ${secret.nacos.serverAddr}\n      namespace: ${secret.nacos.namespace}\n      username: ${secret.nacos.username}\n      password: ${secret.nacos.password}\n\n\n## springdoc 配置\nspringdoc:\n  info:\n    title: ${application.title}${secret.swagger.title}\n    license:\n      name: ${secret.swagger.license}\n      url: ${secret.swagger.licenseUrl}\n    contact:\n      name: ${secret.swagger.author.name}\n      email: ${secret.swagger.author.email}\n    version: ${secret.swagger.version}\n    description: ${application.title}${secret.swagger.title}\n    termsOfService: ${secret.swagger.licenseUrl}\n  api-docs:\n    path: /v3/api-docs\n    enabled: true\n  group-configs:\n    - group: \'default\'\n      paths-to-match: \'/**\'\n      packages-to-scan:\n        - com.xueyi\n      display-name: ${application.title}-${secret.swagger.version}', 'dd79c4a93f12118fc9ca7ee85f2cb392', '2025-03-05 21:32:45', '2025-03-05 21:50:51', 'nacos', '192.168.31.128', '', 'a3d0a606-52e7-43e3-a194-6052aa159aff', '通用动态多数据源配置', '', '', 'yaml', '', '');
INSERT INTO `config_info` VALUES (4, 'xueyi-gateway-dev.yml', 'DEFAULT_GROUP', '# spring配置\nspring:\n  data:\n    redis:\n      host: ${secret.redis.host}\n      port: ${secret.redis.port}\n      password: ${secret.redis.password}\n      database: ${secret.redis.database}\n  cloud:\n    gateway:\n      discovery:\n        locator:\n          # 启用服务发现\n          enabled: true\n          lowerCaseServiceId: true\n      # 网关路由\n      routes:\n        # 认证中心\n        - id: xueyi-auth\n          uri: lb://xueyi-auth\n          predicates:\n            - Path=/auth/**\n          filters:\n            # 验证码处理\n            - CacheRequestFilter\n            - ValidateCodeFilter\n            - StripPrefix=1\n        # 代码生成\n        - id: xueyi-gen\n          uri: lb://xueyi-gen\n          predicates:\n            - Path=/code/**\n          filters:\n            - StripPrefix=1\n        # 定时任务\n        - id: xueyi-job\n          uri: lb://xueyi-job\n          predicates:\n            - Path=/schedule/**\n          filters:\n            - StripPrefix=1\n        # 系统模块\n        - id: xueyi-system\n          uri: lb://xueyi-system\n          predicates:\n            - Path=/system/**\n          filters:\n            - StripPrefix=1\n        # 租户模块\n        - id: xueyi-tenant\n          uri: lb://xueyi-tenant\n          predicates:\n            - Path=/tenant/**\n          filters:\n            - StripPrefix=1\n        # 文件服务\n        - id: xueyi-file\n          uri: lb://xueyi-file\n          predicates:\n            - Path=/file/**\n          filters:\n            - StripPrefix=1\n\n# knife4j 网关聚合\nknife4j:\n  gateway:\n    enabled: true\n    # 指定服务发现的模式聚合微服务文档，并且是默认 default 分组\n    strategy: discover\n    discover:\n      # OpenAPI 3.0 规范 \n      version: openapi3\n      enabled: true\n      # 需要排除的微服务\n      excluded-services:\n        - xueyi-auth\n        - xueyi-monitor\n\n# 安全配置\nsecurity:\n  # 验证码\n  captcha:\n    enabled: true\n    type: math\n  # 防止XSS攻击\n  xss:\n    enabled: true\n    excludeUrls:\n      - /system/notice\n  # 不校验白名单\n  ignore:\n    whites:\n      - /code\n      - /actuator/*\n      - /auth/oauth2/token\n      - /auth/logout\n      - /doc.html\n      - /webjars/**\n      - /v3/api-docs/*\n      - /*/v3/api-docs\n      - /auth/token/register\n      - /system/admin/login/admin/getEnterpriseByDomainName\n      - /csrf', '6ad03e1989a42b14ba52696c696f098e', '2025-03-05 21:38:41', '2025-03-05 21:51:19', 'nacos', '192.168.31.128', '', 'a3d0a606-52e7-43e3-a194-6052aa159aff', '网关模块', '', '', 'yaml', '', '');
INSERT INTO `config_info` VALUES (5, 'xueyi-auth-dev.yml', 'DEFAULT_GROUP', '# spring配置\nspring:\n  data:\n    redis:\n      host: ${secret.redis.host}\n      port: ${secret.redis.port}\n      password: ${secret.redis.password}\n      database: ${secret.redis.database}', '8207f38b52edc88d6df284af2bac31af', '2025-03-05 21:39:35', '2025-03-05 21:40:14', 'nacos', '192.168.31.128', '', 'a3d0a606-52e7-43e3-a194-6052aa159aff', '认证中心', '', '', 'yaml', '', '');
INSERT INTO `config_info` VALUES (6, 'xueyi-monitor-dev.yml', 'DEFAULT_GROUP', '# spring配置\nspring:\n  security:\n    user:\n      name: ${secret.monitor.name}\n      password: ${secret.monitor.password}\n  boot:\n    admin:\n      ui:\n        title: ${secret.security.title}', '161181311076078db783b03d94e928a3', '2025-03-05 21:40:34', '2025-03-05 21:41:09', 'nacos', '192.168.31.128', '', 'a3d0a606-52e7-43e3-a194-6052aa159aff', '监控中心', '', '', 'yaml', '', '');
INSERT INTO `config_info` VALUES (7, 'xueyi-tenant-dev.yml', 'DEFAULT_GROUP', 'xueyi:\n  # 租户配置\n  tenant:\n    # 公共表配置\n    common-table:\n      - sys_menu\n      - sys_module\n    # 非租户表配置\n    exclude-table:\n      - te_tenant\n      - te_strategy\n      - te_source\n\n# 租户库必需数据表\nsource-config:\n  slave-table:\n    - sys_dept\n    - sys_post\n    - sys_user\n    - sys_user_post_merge\n    - sys_role\n    - sys_role_module_merge\n    - sys_role_menu_merge\n    - sys_role_dept_merge\n    - sys_role_post_merge\n    - sys_organize_role_merge\n    - sys_operate_log\n    - sys_login_log\n    - sys_notice\n    - sys_notice_log\n    - sys_job_log\n    - sys_file\n    - sys_file_folder\n\n# mybatis-plus配置\nmybatis-plus:\n  # 搜索指定包别名\n  typeAliasesPackage: com.xueyi.tenant\n  # 配置mapper的扫描，找到所有的mapper.xml映射文件\n  mapperLocations: classpath:mapper/**/*.xml\n\n#seata配置\nseata:\n  # 服务配置项\n  service:\n    # 虚拟组和分组的映射\n    vgroup-mapping:\n      xueyi-tenant-group: default', '4a5c46a26279829866daaabe28e9afbe', '2025-03-05 21:41:38', '2025-03-05 21:42:25', 'nacos', '192.168.31.128', '', 'a3d0a606-52e7-43e3-a194-6052aa159aff', '租户模块', '', '', 'yaml', '', '');
INSERT INTO `config_info` VALUES (8, 'xueyi-system-dev.yml', 'DEFAULT_GROUP', 'xueyi:\r\n  # 租户配置\r\n  tenant:\r\n    # 公共表配置\r\n    common-table:\r\n      - sys_menu\r\n      - sys_module\r\n    # 非租户表配置\r\n    exclude-table:\r\n      - te_tenant\r\n      - te_strategy\r\n      - sys_auth_group\r\n      - sys_auth_group_menu_merge\r\n      - sys_auth_group_module_merge\r\n      - sys_tenant_auth_group_merge\r\n      - sys_oauth_client\r\n  # MQ配置\r\n  mq:\r\n    # Rabbit配置\r\n    rabbit:\r\n      # 默认地址  \r\n      host: ${secret.rabbitmq.host}\r\n      # 默认端口\r\n      port: ${secret.rabbitmq.port}\r\n      # 账号\r\n      username: ${secret.rabbitmq.username}\r\n      # 密码\r\n      password: ${secret.rabbitmq.password}\r\n      # 前缀标识\r\n      prefix: ${secret.rabbitmq.prefix}\r\n      # 环境标识\r\n      evn: ${secret.rabbitmq.evn}\r\n      # 交换机配置\r\n      exchangeInfos:\r\n          # 交换机类型（direct直连交换机 topic主题交换机 fanout扇出交换机 headers头交换机 x-delayed-message延时消息交换机）\r\n        - type: x-delayed-message\r\n          # 交换机名称\r\n          name: sto_system\r\n          # 是否持久化\r\n          durable: true\r\n          # 自定义参数\r\n          params: \r\n            x-delayed-type: direct\r\n          # 队列配置\r\n          queueInfos:\r\n              # 队列路由键名（当前仅用于示例，请自定义）\r\n            - routingKey: sys_dict_refresh\r\n              # 队列名\r\n              name: sys_dict_refresh\r\n              # 是否持久化\r\n              durable: true\r\n\r\nspring:\r\n  # RabbitMQ 配置\r\n  rabbitmq:\r\n    # 地址  \r\n    host: ${secret.rabbitmq.host}\r\n    # 端口\r\n    port: ${secret.rabbitmq.port}\r\n    # 账号\r\n    username: ${secret.rabbitmq.username}\r\n    # 密码\r\n    password: ${secret.rabbitmq.password}\r\n    # 消息监听器配置\r\n    listener:\r\n      # 消息监听容器类型，默认 simple\r\n      type: simple\r\n      simple:\r\n        # 消息确认模式，none、manual和auto\r\n        acknowledge-mode: none\r\n        # 应用启动时是否启动容器，默认true\r\n        auto-startup: true\r\n        # listener最小消费者数\r\n        concurrency: 10\r\n        # listener最大消费者数\r\n        max-concurrency: 100\r\n        # 一个消费者最多可处理的nack消息数量\r\n        prefetch: 10\r\n        # 被拒绝的消息是否重新入队,默认true\r\n        default-requeue-rejected: true\r\n        # 如果容器声明的队列不可用，是否失败；或如果在运行时删除一个或多个队列，是否停止容器,默认true\r\n        missing-queues-fatal: true\r\n        # 空闲容器事件应多久发布一次\r\n        idle-event-interval: 10\r\n        # 重试配置\r\n        retry:\r\n          # 是否开启消费者重试，默认false\r\n          enabled: true\r\n          # 尝试发送消息的时间间隔，默认1000ms\r\n          initial-interval: 5000ms\r\n          # 最大重试次数,默认3\r\n          max-attempts: 3\r\n          # 最大重试间隔，默认10000ms\r\n          max-interval: 10000ms\r\n          # 应用于前一个重试间隔的乘数，间隔时间*乘子=下一次的间隔时间，不能超过max-interval\r\n          # 以initial-interval=5000ms,multiplier=2为例：第一次间隔 5 秒，第二次间隔 10 秒，以此类推\r\n          multiplier: 1\r\n          # 重试是无状态还是有状态,默认true\r\n          stateless: true\r\n\r\n# mybatis-plus配置\r\nmybatis-plus:\r\n  # 搜索指定包别名\r\n  typeAliasesPackage: com.xueyi.system\r\n  # 配置mapper的扫描，找到所有的mapper.xml映射文件\r\n  mapperLocations: classpath:mapper/**/*.xml\r\n\r\n#seata配置\r\nseata:\r\n  # 服务配置项\r\n  service:\r\n    # 虚拟组和分组的映射\r\n    vgroup-mapping:\r\n      xueyi-system-group: default\r\n', '01abfc750a0c942167651c40d088531d', '2025-03-05 21:42:56', '2025-03-05 21:42:56', 'nacos', '192.168.31.128', '', 'a3d0a606-52e7-43e3-a194-6052aa159aff', '系统模块', NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (9, 'xueyi-gen-dev.yml', 'DEFAULT_GROUP', 'xueyi:\n  # 租户配置\n  tenant:\n    # 非租户表配置\n    exclude-table:\n      - gen_table\n      - gen_table_column\n\n# mybatis-plus配置\nmybatis-plus:\n  # 搜索指定包别名\n  typeAliasesPackage: com.xueyi.gen\n  # 配置mapper的扫描，找到所有的mapper.xml映射文件\n  mapperLocations: classpath:mapper/**/*.xml\n  configuration:\n    jdbc-type-for-null: \'null\'\n\n# 代码生成\ngen: \n  # 作者\n  author: xueyi\n  # ui路径（空代表生成在后端主路径下，可设置为ui项目地址如：C:UsersxueyiMultiSaas-UI）\n  uiPath: \n  # 自动去除表前缀，默认是true\n  autoRemovePre: true\n  # 数据库映射\n  data-base:\n    # 字符串类型\n    type-str: [\"char\", \"varchar\", \"nvarchar\", \"varchar2\"]\n    # 文本类型\n    type-text: [\"tinytext\", \"text\", \"mediumtext\", \"longtext\"]\n    # 日期类型\n    type-date: [\"datetime\", \"time\", \"date\", \"timestamp\"]\n    # 时间类型\n    type-time: [\"datetime\", \"time\", \"date\", \"timestamp\"]\n    # 数字类型\n    type-number: [\"tinyint\", \"smallint\", \"mediumint\", \"int\", \"number\", \"integer\"]\n    # 长数字类型\n    type-long: [\"bigint\"]\n    # 浮点类型\n    type-float: [\"float\", \"double\", \"decimal\"]\n  # 字段配置\n  operate:\n    # 隐藏详情显示\n    not-view: [\"id\", \"createBy\", \"createTime\", \"updateBy\", \"updateTime\"]\n    # 隐藏新增显示\n    not-insert: [\"id\", \"createBy\", \"createTime\", \"updateBy\", \"updateTime\"]\n    # 隐藏编辑显示\n    not-edit: [\"id\", \"createBy\", \"createTime\", \"updateBy\", \"updateTime\"]\n    # 隐藏列表显示\n    not-list: [\"id\", \"createBy\", \"createTime\", \"updateBy\", \"updateTime\", \"remark\"]\n    # 隐藏查询显示\n    not-query: [\"id\", \"sort\", \"createBy\", \"createTime\", \"updateBy\", \"updateTime\", \"remark\"]\n    # 隐藏导入显示\n    not-import: [\"id\", \"createBy\", \"createTime\", \"updateBy\", \"updateTime\"]\n    # 隐藏导出显示\n    not-export: [\"id\", \"sort\", \"createBy\", \"updateBy\"]\n  # 基类配置\n  entity:\n    # 必定隐藏字段（前后端均隐藏）\n    must-hide: [\"delFlag\", \"tenantId\", \"ancestors\"]\n    # 后端基类\n    back:\n      base: [\"id\", \"name\", \"status\", \"sort\", \"remark\", \"createBy\", \"createTime\", \"updateBy\", \"updateTime\", \"delFlag\"]\n      tree: [\"parentId\", \"ancestors\", \"level\"]\n      tenant: [\"tenantId\"]\n      common: [\"isCommon\"]\n    # 前端基类\n    front:\n      base: [\"sort\", \"remark\", \"createBy\", \"createName\", \"createTime\", \"updateBy\", \"updateName\", \"updateTime\", \"delFlag\"]\n      tree: [\"parentId\", \"ancestors\", \"level\"]\n      tenant: [\"tenantId\"]\n      common: [\"isCommon\"]\n  # 表前缀（与remove-lists对应）\n  dict-type-remove: [\"sys_\", \"te_\"]\n  # 表更替配置\n  remove-lists:\n      # 表前缀（生成类名不会包含表前缀）\n    - prefix: sys_\n      # 默认生成包路径 system 需改成自己的模块名称 如 system monitor tool\n      rdPackageName: com.xueyi.system\n      fePackageName: system\n      backPackageRoute: /xueyi-modules/xueyi-system\n    - prefix: te_\n      rdPackageName: com.xueyi.tenant\n      fePackageName: tenant\n      backPackageRoute: /xueyi-modules/xueyi-tenant', '0143096d6fec3cee1821b17db92a14c5', '2025-03-05 21:43:46', '2025-03-05 21:44:32', 'nacos', '192.168.31.128', '', 'a3d0a606-52e7-43e3-a194-6052aa159aff', '代码生成', '', '', 'yaml', '', '');
INSERT INTO `config_info` VALUES (10, 'xueyi-job-dev.yml', 'DEFAULT_GROUP', '# mybatis-plus配置\r\nmybatis-plus:\r\n  # 搜索指定包别名\r\n  typeAliasesPackage: com.xueyi.job\r\n  # 配置mapper的扫描，找到所有的mapper.xml映射文件\r\n  mapperLocations: classpath:mapper/**/*.xml\r\n\r\n# seata配置\r\nseata:\r\n  # 服务配置项\r\n  service:\r\n    # 虚拟组和分组的映射\r\n    vgroup-mapping:\r\n      xueyi-job-group: default\r\n', '01abfc750a0c942167651c40d088531d', '2025-03-05 21:45:03', '2025-03-05 21:45:03', 'nacos', '192.168.31.128', '', 'a3d0a606-52e7-43e3-a194-6052aa159aff', '定时任务', NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (11, 'xueyi-file-dev.yml', 'DEFAULT_GROUP', '# spring配置\r\nspring:\r\n  data:\r\n    redis:\r\n      host: ${secret.redis.host}\r\n      port: ${secret.redis.port}\r\n      password: ${secret.redis.password}\r\n      database: ${secret.redis.database}\r\n\r\n# 本地文件上传\r\nfile:\r\n  domain: http://127.0.0.1:9300\r\n  path: D:/xueyi/uploadPath\r\n  prefix: /statics\r\n\r\n# FastDFS配置\r\nfdfs:\r\n  domain: http://8.129.231.12\r\n  soTimeout: 3000\r\n  connectTimeout: 2000\r\n  trackerList: 8.129.231.12:22122\r\n\r\n# Minio配置\r\nminio:\r\n  url: http://8.129.231.12:9000\r\n  accessKey: minioadmin\r\n  secretKey: minioadmin\r\n  bucketName: test\r\n\r\nsecurity:\r\n  oauth2:\r\n    ignore:\r\n      whites:\r\n        routine:\r\n          - ${file.prefix}/**\r\n', '05ab88fb98453f3a811b785145662131', '2025-03-05 21:46:44', '2025-03-05 21:46:44', 'nacos', '192.168.31.128', '', 'a3d0a606-52e7-43e3-a194-6052aa159aff', '文件服务', NULL, NULL, 'yaml', NULL, '');
INSERT INTO `config_info` VALUES (12, 'sentinel-xueyi-gateway', 'DEFAULT_GROUP', '[\n    {\n        \"resource\": \"xueyi-auth\",\n        \"count\": 500,\n        \"grade\": 1,\n        \"limitApp\": \"default\",\n        \"strategy\": 0,\n        \"controlBehavior\": 0\n    },\n	{\n        \"resource\": \"xueyi-system\",\n        \"count\": 1000,\n        \"grade\": 1,\n        \"limitApp\": \"default\",\n        \"strategy\": 0,\n        \"controlBehavior\": 0\n    },\n	{\n        \"resource\": \"xueyi-tenant\",\n        \"count\": 500,\n        \"grade\": 1,\n        \"limitApp\": \"default\",\n        \"strategy\": 0,\n        \"controlBehavior\": 0\n    },\n	{\n        \"resource\": \"xueyi-gen\",\n        \"count\": 200,\n        \"grade\": 1,\n        \"limitApp\": \"default\",\n        \"strategy\": 0,\n        \"controlBehavior\": 0\n    },\n	{\n        \"resource\": \"xueyi-job\",\n        \"count\": 300,\n        \"grade\": 1,\n        \"limitApp\": \"default\",\n        \"strategy\": 0,\n        \"controlBehavior\": 0\n    }\n]', 'f4a5d3ca1c2c47c86a85fcc2acf4ebce', '2025-03-05 21:47:54', '2025-03-05 21:49:10', 'nacos', '192.168.31.128', '', 'a3d0a606-52e7-43e3-a194-6052aa159aff', '限流策略', '', '', 'json', '', '');

-- ----------------------------
-- Table structure for config_info_beta
-- ----------------------------
DROP TABLE IF EXISTS `config_info_beta`;
CREATE TABLE `config_info_beta`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT 'group_id',
  `app_name` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT 'content',
  `beta_ips` varchar(1024) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT 'betaIps',
  `md5` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL COMMENT 'source user',
  `src_ip` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT 'source ip',
  `tenant_id` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT '' COMMENT '租户字段',
  `encrypted_data_key` varchar(1024) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL DEFAULT '' COMMENT '密钥',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfobeta_datagrouptenant`(`data_id` ASC, `group_id` ASC, `tenant_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_bin COMMENT = 'config_info_beta' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_info_beta
-- ----------------------------

-- ----------------------------
-- Table structure for config_info_gray
-- ----------------------------
DROP TABLE IF EXISTS `config_info_gray`;
CREATE TABLE `config_info_gray`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT 'group_id',
  `content` longtext CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT 'content',
  `md5` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT 'md5',
  `src_user` text CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL COMMENT 'src_user',
  `src_ip` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT 'src_ip',
  `gmt_create` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT 'gmt_create',
  `gmt_modified` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT 'gmt_modified',
  `app_name` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT 'app_name',
  `tenant_id` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT 'tenant_id',
  `gray_name` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT 'gray_name',
  `gray_rule` text CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT 'gray_rule',
  `encrypted_data_key` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT 'encrypted_data_key',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfogray_datagrouptenantgray`(`data_id` ASC, `group_id` ASC, `tenant_id` ASC, `gray_name` ASC) USING BTREE,
  INDEX `idx_dataid_gmt_modified`(`data_id` ASC, `gmt_modified` ASC) USING BTREE,
  INDEX `idx_gmt_modified`(`gmt_modified` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = 'config_info_gray' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_info_gray
-- ----------------------------

-- ----------------------------
-- Table structure for config_info_tag
-- ----------------------------
DROP TABLE IF EXISTS `config_info_tag`;
CREATE TABLE `config_info_tag`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT 'group_id',
  `tenant_id` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `tag_id` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT 'tag_id',
  `app_name` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT 'content',
  `md5` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL COMMENT 'source user',
  `src_ip` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT 'source ip',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfotag_datagrouptenanttag`(`data_id` ASC, `group_id` ASC, `tenant_id` ASC, `tag_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_bin COMMENT = 'config_info_tag' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_info_tag
-- ----------------------------

-- ----------------------------
-- Table structure for config_tags_relation
-- ----------------------------
DROP TABLE IF EXISTS `config_tags_relation`;
CREATE TABLE `config_tags_relation`  (
  `id` bigint NOT NULL COMMENT 'id',
  `tag_name` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT 'tag_name',
  `tag_type` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT 'tag_type',
  `data_id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT 'group_id',
  `tenant_id` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `nid` bigint NOT NULL AUTO_INCREMENT COMMENT 'nid, 自增长标识',
  PRIMARY KEY (`nid`) USING BTREE,
  UNIQUE INDEX `uk_configtagrelation_configidtag`(`id` ASC, `tag_name` ASC, `tag_type` ASC) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_bin COMMENT = 'config_tag_relation' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_tags_relation
-- ----------------------------

-- ----------------------------
-- Table structure for group_capacity
-- ----------------------------
DROP TABLE IF EXISTS `group_capacity`;
CREATE TABLE `group_capacity`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `group_id` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL DEFAULT '' COMMENT 'Group ID，空字符表示整个集群',
  `quota` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '配额，0表示使用默认值',
  `usage` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '使用量',
  `max_size` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
  `max_aggr_count` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '聚合子配置最大个数，，0表示使用默认值',
  `max_aggr_size` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
  `max_history_count` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '最大变更历史数量',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_group_id`(`group_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_bin COMMENT = '集群、各Group容量信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of group_capacity
-- ----------------------------

-- ----------------------------
-- Table structure for his_config_info
-- ----------------------------
DROP TABLE IF EXISTS `his_config_info`;
CREATE TABLE `his_config_info`  (
  `id` bigint UNSIGNED NOT NULL COMMENT 'id',
  `nid` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'nid, 自增标识',
  `data_id` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT 'group_id',
  `app_name` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT 'content',
  `md5` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL COMMENT 'source user',
  `src_ip` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT 'source ip',
  `op_type` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT 'operation type',
  `tenant_id` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT '' COMMENT '租户字段',
  `encrypted_data_key` varchar(1024) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL DEFAULT '' COMMENT '密钥',
  `publish_type` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT 'formal' COMMENT 'publish type gray or formal',
  `gray_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT 'gray name',
  `ext_info` longtext CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL COMMENT 'ext info',
  PRIMARY KEY (`nid`) USING BTREE,
  INDEX `idx_gmt_create`(`gmt_create` ASC) USING BTREE,
  INDEX `idx_gmt_modified`(`gmt_modified` ASC) USING BTREE,
  INDEX `idx_did`(`data_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_bin COMMENT = '多租户改造' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of his_config_info
-- ----------------------------

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `role` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'role',
  `resource` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'resource',
  `action` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'action',
  UNIQUE INDEX `uk_role_permission`(`role` ASC, `resource` ASC, `action` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permissions
-- ----------------------------

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'username',
  `role` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'role',
  UNIQUE INDEX `idx_user_role`(`username` ASC, `role` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('nacos', 'ROLE_ADMIN');

-- ----------------------------
-- Table structure for tenant_capacity
-- ----------------------------
DROP TABLE IF EXISTS `tenant_capacity`;
CREATE TABLE `tenant_capacity`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `tenant_id` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL DEFAULT '' COMMENT 'Tenant ID',
  `quota` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '配额，0表示使用默认值',
  `usage` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '使用量',
  `max_size` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
  `max_aggr_count` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '聚合子配置最大个数',
  `max_aggr_size` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
  `max_history_count` int UNSIGNED NOT NULL DEFAULT 0 COMMENT '最大变更历史数量',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_tenant_id`(`tenant_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_bin COMMENT = '租户容量信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tenant_capacity
-- ----------------------------

-- ----------------------------
-- Table structure for tenant_info
-- ----------------------------
DROP TABLE IF EXISTS `tenant_info`;
CREATE TABLE `tenant_info`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
  `kp` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL COMMENT 'kp',
  `tenant_id` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `tenant_name` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT '' COMMENT 'tenant_name',
  `tenant_desc` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT 'tenant_desc',
  `create_source` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NULL DEFAULT NULL COMMENT 'create_source',
  `gmt_create` bigint NOT NULL COMMENT '创建时间',
  `gmt_modified` bigint NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_tenant_info_kptenantid`(`kp` ASC, `tenant_id` ASC) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_bin COMMENT = 'tenant_info' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tenant_info
-- ----------------------------
INSERT INTO `tenant_info` VALUES (1, '1', 'a3d0a606-52e7-43e3-a194-6052aa159aff', 'xueyi-dev', '雪忆天堂开发分支', 'nacos', 1741180177201, 1741180177201);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'username',
  `password` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'password',
  `enabled` tinyint(1) NOT NULL COMMENT 'enabled',
  PRIMARY KEY (`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('nacos', '$2a$10$GgmkQbSCq7hawrzFgIw16e4p.uuyWcSn0cekSw1wgUztdgN9aARlm', 1);

SET FOREIGN_KEY_CHECKS = 1;
