package com.xueyi.common.cache.utils;

import com.xueyi.common.cache.service.CacheService;
import com.xueyi.common.core.utils.core.MapUtil;
import com.xueyi.common.core.utils.core.ObjectUtil;
import com.xueyi.common.core.utils.core.SpringUtil;
import com.xueyi.common.core.web.model.SysEnterprise;
import com.xueyi.common.redis.service.RedisService;
import com.xueyi.tenant.api.tenant.constant.TenantConstants;

import java.util.Map;

/**
 * 企业缓存管理工具类
 *
 * @author xueyi
 */
public class EnterpriseUtil {

    /**
     * 获取企业缓存信息
     *
     * @param id 企业Id
     * @return 企业信息对象
     */
    public static SysEnterprise getEnterpriseCache(Long id) {
        TenantConstants.CacheType cacheType = TenantConstants.CacheType.TE_TENANT_KEY;
        return SpringUtil.getBean(CacheService.class).getCacheObject(cacheType.getCode(), cacheType.getIsTenant(), cacheType.getConsumer(), id.toString());
    }

    /**
     * 获取企业缓存信息Map
     *
     * @return 企业信息对象Map
     */
    public static Map<String, SysEnterprise> getEnterpriseCache() {
        TenantConstants.CacheType cacheType = TenantConstants.CacheType.TE_TENANT_KEY;
        RedisService redisService = SpringUtil.getBean(RedisService.class);
        Map<String, SysEnterprise> cacheMap = redisService.getCacheMap(cacheType.getCode());
        if (MapUtil.isEmpty(cacheMap)) {
            SpringUtil.getBean(CacheService.class).refreshCache(cacheType.getConsumer());
        }
        return redisService.getCacheMap(cacheType.getCode());
    }

    /**
     * 通过企业账号获取企业缓存信息
     *
     * @param enterpriseName 企业账号
     * @return 企业信息对象
     */
    public static SysEnterprise getEnterpriseCache(String enterpriseName) {
        TenantConstants.CacheType cacheType = TenantConstants.CacheType.TE_ENTERPRISE_SYSTEM_NAME_KEY;
        Long enterpriseId = SpringUtil.getBean(CacheService.class).getCacheObject(cacheType.getCode(), cacheType.getIsTenant(), cacheType.getConsumer(), enterpriseName);
        if (ObjectUtil.isNull(enterpriseId)) {
            return null;
        }
        return getEnterpriseCache(enterpriseId);
    }
}
