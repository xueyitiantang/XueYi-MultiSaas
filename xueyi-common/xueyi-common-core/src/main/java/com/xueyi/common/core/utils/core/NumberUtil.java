package com.xueyi.common.core.utils.core;

import com.xueyi.common.core.utils.core.pool.NumberPool;

import java.math.BigDecimal;

/**
 * 数字工具类
 *
 * @author xueyi
 */
public class NumberUtil extends cn.hutool.core.util.NumberUtil implements NumberPool {

    public static int compare(BigDecimal x, BigDecimal y) {
        return x.compareTo(y);
    }
}