package com.xueyi.common.core.web.error;

import lombok.Data;

/**
 * 错误码 基础数据对象
 *
 * @author xueyi
 */
@Data
public class ErrorCode {

    /** 错误码 */
    private final Integer code;

    /** 错误提示 */
    private final String msg;

    public ErrorCode(Integer code, String message) {
        this.code = code;
        this.msg = message;
    }
}
