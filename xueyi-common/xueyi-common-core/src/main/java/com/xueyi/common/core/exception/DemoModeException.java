package com.xueyi.common.core.exception;

import lombok.NoArgsConstructor;

import java.io.Serial;

/**
 * 演示模式异常
 *
 * @author xueyi
 */
@NoArgsConstructor
public class DemoModeException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 1L;

}
