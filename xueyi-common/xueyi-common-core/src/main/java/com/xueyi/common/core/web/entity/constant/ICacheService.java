package com.xueyi.common.core.web.entity.constant;

import com.xueyi.common.core.utils.core.NumberUtil;
import com.xueyi.common.core.utils.core.StrUtil;

import java.util.concurrent.TimeUnit;

/**
 * 缓存通用规则
 *
 * @author xueyi
 */
public interface ICacheService {

    /**
     * 获取缓存名称
     *
     * @return 缓存名称
     */
    String getCode();

    /**
     * 获取缓存过期时间
     *
     * @return 缓存过期时间
     */
    default Long getExpire() {
        return (long) NumberUtil.One;
    }

    /**
     * 获取缓存过期时间单位
     *
     * @return 缓存过期时间单位
     */
    default TimeUnit getTimeUnit() {
        return TimeUnit.DAYS;
    }

    /**
     * 获取缓存名称
     *
     * @param cacheNames 参数
     * @return 缓存名称
     */
    default String getCacheName(Object... cacheNames) {
        return StrUtil.format(getCode(), cacheNames);
    }
}

