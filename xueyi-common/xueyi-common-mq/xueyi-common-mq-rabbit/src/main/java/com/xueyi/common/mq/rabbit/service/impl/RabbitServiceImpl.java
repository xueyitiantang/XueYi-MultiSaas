package com.xueyi.common.mq.rabbit.service.impl;

import com.xueyi.common.core.constant.basic.EvnConstants;
import com.xueyi.common.core.utils.core.StrUtil;
import com.xueyi.common.mq.rabbit.config.properties.RabbitExchangeProperties;
import com.xueyi.common.mq.rabbit.service.RabbitService;
import jakarta.annotation.Resource;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * RabbitMQ消息 工具类
 *
 * @author xueyi
 **/
@Slf4j
@Component
public class RabbitServiceImpl implements RabbitService {

    @Getter
    @Resource
    private RabbitTemplate rabbitTemplate;

    @Resource
    private RabbitExchangeProperties rabbitExchangeProperties;

    @Bean("rabbitMQEvn")
    public String rabbitMQEvn() {
        return getEvnPrefix();
    }

    private String getEvnPrefix() {
        return rabbitExchangeProperties.getEvnPrefix();
    }

    public String getEvnPrefix(String key) {
        return getEvnPrefix() + key;
    }

    @Override
    @SneakyThrows
    public void convertAndSend(String exchangeName, String routingKey, Object object) {
        rabbitTemplate.convertAndSend(getEvnPrefix(exchangeName), getEvnPrefix(routingKey), object);
    }

    @Override
    @SneakyThrows
    public void convertAndSendWithEvn(EvnConstants.EvnType evn, String exchangeName, String routingKey, Object object) {
        String prefix = StrUtil.isNotBlank(rabbitExchangeProperties.getPrefix()) ? StrUtil.format("{}.", rabbitExchangeProperties.getPrefix()) : StrUtil.EMPTY;
        rabbitTemplate.convertAndSend(StrUtil.format("{}{}.{}", prefix, evn.getCode(), exchangeName), StrUtil.format("{}{}.{}", prefix, evn.getCode(), routingKey), object);
    }

    @Override
    @SneakyThrows
    public void convertAndSendDelayed(String exchangeName, String routingKey, Object object, long delayInSeconds, long expirationSeconds) {
        log.info("发送延时消息：交换机名称：{}，路由键：{}，消息内容：{}，延时时间：{}秒", getEvnPrefix(exchangeName), getEvnPrefix(routingKey), object, delayInSeconds);
        rabbitTemplate.convertAndSend(getEvnPrefix(exchangeName), getEvnPrefix(routingKey), object,
                message -> {
                    // 设置延时时间（毫秒）
                    message.getMessageProperties().setDelayLong(delayInSeconds * 1000);
                    // 设置消息持续时间（毫秒）
                    message.getMessageProperties().setExpiration(String.valueOf(expirationSeconds * 1000));
                    return message;
                });
    }
}
