package com.xueyi.mqtt.client.callback;

import com.xueyi.mqtt.basicClient.callback.MqttBaseSendCallBack;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * MQTT发送客户端的回调 | 默认
 *
 * @author xueyi
 */
@Slf4j
@Component
public class MqttSendCallBack extends MqttBaseSendCallBack {
}
